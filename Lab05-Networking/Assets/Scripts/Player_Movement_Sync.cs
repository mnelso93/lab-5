﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_Movement_Sync : NetworkBehaviour {

	[SyncVar] Vector3 syncPos;

	[SerializeField] Transform myTransform;
	[SerializeField] float lerpRate = 15;

	
	// Update is called once per frame
	void FixedUpdate () 
	{
		TransmitPos ();
		LerpPostion ();
	}

	void LerpPostion ()
	{
		if (!isLocalPlayer)
		{
			myTransform.position = Vector3.Lerp (myTransform.position, syncPos, Time.deltaTime * lerpRate);
		}
	}

	[Command]
	void CmdProvidePositionToServer(Vector3 pos)
	{
		syncPos = pos;
	}

	[Client]
	void TransmitPos ()
	{
		if (isLocalPlayer) 
		{
			CmdProvidePositionToServer (myTransform.position);
		}
	}
}
