﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_Network_Setup : NetworkBehaviour {

	[SerializeField] Camera characterCam;
	[SerializeField] AudioListener audioListener;

	// Use this for initialization
	void Start () 
	{
		if (isLocalPlayer) 
		{
			GameObject.Find("Scene Camera").SetActive(false);
			GetComponent<CharacterController> ().enabled = true;
			GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ().enabled = true;
			characterCam.enabled = true;
			audioListener.enabled = true;
		}
	}
}
