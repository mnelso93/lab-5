﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_Rotation_Sync : NetworkBehaviour {

	[SyncVar] Quaternion syncPlayerRotation;
	[SyncVar] Quaternion syncCamRotation;

	[SerializeField] Transform playerTransform;
	[SerializeField] Transform camTransform;
	[SerializeField] float lerpRate = 15;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transformRotation ();
		LerpRotations ();
		
	}

	void LerpRotations()
	{
		if (!isLocalPlayer) 
		{
			playerTransform.rotation = Quaternion.Lerp (playerTransform.rotation, syncPlayerRotation, Time.deltaTime * lerpRate);
			camTransform.rotation = Quaternion.Lerp (camTransform.rotation, syncCamRotation, Time.deltaTime * lerpRate);
		}
	}

	[Command]
	void CmdProvideRoationToServer(Quaternion playerRot, Quaternion camRot)
	{
		syncPlayerRotation = playerRot;
		syncCamRotation = camRot;
	}

	[Client]
	void transformRotation()
	{
		if (isLocalPlayer) 
		{
			CmdProvideRoationToServer (playerTransform.rotation, camTransform.rotation);
		}
	}
}
